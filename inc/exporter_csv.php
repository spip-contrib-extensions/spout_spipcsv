<?php

/***************************************************************************\
 *  SPIP, Système de publication pour l'internet                           *
 *                                                                         *
 *  Copyright © avec tendresse depuis 2001                                 *
 *  Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribué sous licence GNU/GPL.     *
\***************************************************************************/

/**
 * Gestion d'export de données au format CSV
 *
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/charsets');
include_spip('inc/filtres');
include_spip('inc/texte');

require_once find_in_path('lib/Spout/Autoloader/autoload.php');
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Common\Entity\Style\Color;

/**
 * Exporte une ressource sous forme de fichier CSV
 *
 * La ressource peut etre un tableau ou une resource SQL issue d'une requete
 * Le nom du fichier est défini en fonction du titre s'il n'est pas indiqué dans les options.
 * L'extension est choisie en fonction du délimiteur si elle n'est pas indiquée dans les options :
 * - si on utilise ',' c'est un vrai csv avec extension csv
 * - si on utilise ';' ou tabulation c'est pour E*cel, et on exporte avec une extension .xlsx
 *
 * @uses exporter_csv_ligne()
 *
 * @param string $titre
 *    Titre utilisé pour nommer le fichier si celui-ci n'est pas indiqué dans les options
 *    Il peut s'agir d'un texte contenant de la syntaxe SPIP
 * @param array|resource $resource
 * @param array $options
 *   - (string)   fichier   : nom du fichier, par défaut défini en fonction du $titre
 *   - (string)   extension : `csv` | `xls`, par défaut choisie en fonction du délimiteur
 *   - (string)   delim     : `,` | `;` | `\t` | `TAB`
 *   - (array)    entetes   : tableau d'en-tetes pour nommer les colonnes (genere la premiere ligne)
 *   - (bool)     envoyer   : pour envoyer le fichier exporte (permet le telechargement)
 *   - (string)   charset   : charset de l'export si different de celui du site
 *   - (callable) callback  : fonction callback a appeler sur chaque ligne pour mettre en forme/completer les donnees
 * @return string
 */
function inc_exporter_csv_dist($titre, $resource, $options = []) {

	// support ancienne syntaxe
	// inc_exporter_csv_dist($titre, $resource, $delim = ', ', $entetes = null, $envoyer = true)
	if (is_string($options)) {
		$args = func_get_args();
		$options = [];
		foreach ([2 => 'delim', 3 => 'entetes', 4 => 'envoyer'] as $k => $option) {
			if (!empty($args[$k])) {
				$options[$option] = $args[$k];
			}
		}
	}

	$default_options = [
		'fichier' => null, // par défaut = $titre
		'extension' => null, // par défaut = choix auto
		'delim' => ', ',
		'entetes' => null,
		'envoyer' => true,
		'charset' => null,
		'callback' => null,
	];
	$options = array_merge($default_options, $options);

	// Délimiteur
	if ($options['delim'] == 'TAB') {
		$options['delim'] = "\t";
	}
	if (!in_array($options['delim'], [',', ';', "\t"])) {
		$options['delim'] = ',';
	}

	// Nom du fichier : celui indiqué dans les options, sinon le titre
	// Normalisation : uniquement les caractères non spéciaux, tirets, underscore et point + remplacer espaces par underscores
	$filename = $options['fichier'] ?? translitteration(textebrut(typo($titre)));
	$filename = preg_replace([',[^\w\-_\.\s]+,', ',\s+,'], ['', '_'], trim($filename));
	$filename = rtrim($filename, '.');

	$charset = $GLOBALS['meta']['charset'];
	$folder = sous_repertoire(_DIR_CACHE, 'export');
	if ($options['delim'] == ',') {
		$extension = 'csv';
		$writer = WriterEntityFactory::createWriterFromFile("$folder$filename.$extension");
		$writer->openToFile("$folder$filename.$extension");
	} else {
		$extension = 'xlsx';
		$defaultStyle = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(11)
                ->build();
		$writer = WriterEntityFactory::createXLSXWriter();
		$writer->setTempFolder(sous_repertoire(_DIR_CACHE, 'export'));
		$writer->setDefaultRowStyle($defaultStyle)
		       ->openToFile("$folder$filename.$extension");
	}
	$filename = "$filename.$extension";

	if ($options['entetes'] and is_array($options['entetes']) and count($options['entetes'])) {
		$writer->addRow(WriterEntityFactory::createRowFromArray($options['entetes']));
	}

	// on passe par un fichier temporaire qui permet de ne pas saturer la memoire
	// avec les gros exports
	$fichier = $folder . $filename;

	while ($row = is_array($resource) ? array_shift($resource) : sql_fetch($resource)) {
		$writer->addRow(WriterEntityFactory::createRowFromArray($row));
	}

	$writer->close();

	if ($options['envoyer']) {
		header("Content-Type: text/comma-separated-values; charset=$charset");
		header("Content-Disposition: attachment; filename=$filename");
		//non supporte
		//Header("Content-Type: text/plain; charset=$charset");
		header('Content-Length: '.filesize($fichier));
		ob_clean();
		flush();
		readfile($fichier);
	}

	return $fichier;
}
